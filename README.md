# buildtools

set of innius build tools which can be used in ci/cd pipelines. 

## usage 

### get-object-tagging 

Creates environment variables from s3 tagset. This is commonly used in production pipelines which are typically based on a zip file from s3. S3 opbject is provided with tags with version information.

This information is important while deploying services. 

given an object in s3: 

```
s3api get-object-tagging --bucket artifacts.innius.eu-west-1 --key connection-api.zip
{
    "VersionId": "mJyjFGaClShrvvZuA_vrBoVGU1ufUwZv",
    "TagSet": [
        {
            "Key": "version",
            "Value": "v1.0.3"
        }
    ]
}
```

```
buildtools tagenv--bucket artifacts.innius.eu-west-1 --key connection-api.zip

```

#### AWS Codebuild

TODO

```

eval $(buildtools tagenv --key connection-api.zip)

```

results in environment variable: 
```
ARTIFACT_VERSION: v1.0.3
```

