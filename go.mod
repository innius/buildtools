module bitbucket.org/innius/buildtools

go 1.14

require (
	github.com/aws/aws-sdk-go v1.29.21
	github.com/urfave/cli/v2 v2.2.0
)
