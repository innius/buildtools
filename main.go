package main

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"log"
	"os"
	"strings"

	"github.com/urfave/cli/v2"
)

func main() {
	var sess *session.Session
	var region string
	app := &cli.App{
		Name:  "buildtools",
		Usage: "innius build tools for ci/cd pipelines",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "region",
				Aliases:     nil,
				Usage:       "aws region",
				Value:       endpoints.EuWest1RegionID,
				Destination: &region,
			},
		},
		Before: func(c *cli.Context) error {
			sess = session.Must(session.NewSession(aws.NewConfig().WithRegion(region)))
			return nil
		},
		Commands: []*cli.Command{
			{
				Name:  "tagenv",
				Usage: "create environment variables from s3 object tags",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:  "bucket",
						Usage: "s3 bucket name",
						Value: "artifacts.innius.eu-west-1",
					},
					&cli.StringFlag{
						Name:     "key",
						Usage:    "s3 object key",
						Required: true,
					},
					&cli.StringFlag{
						Name:  "prefix",
						Usage: "environment variable prefix",
						Value: "TAGSET",
					},
				},
				Action: func(c *cli.Context) error {
					api := s3.New(sess)

					res, err := api.GetObjectTagging(&s3.GetObjectTaggingInput{
						Key:    aws.String(c.String("key")),
						Bucket: aws.String(c.String("bucket")),
					})
					if err != nil {
						return fmt.Errorf("could not get object tags; %w", err)
					}

					for _, tag := range res.TagSet {
						key := strings.ToUpper(fmt.Sprintf("%s_%s", c.String("prefix"), aws.StringValue(tag.Key)))
						fmt.Printf("%s=%v\n", key, aws.StringValue(tag.Value))
					}
					return nil
				},
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
